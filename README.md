# Demo project - Setup Prometheus Monitoring in Kubernetes cluster 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Deploy Prometheus in local Kubernetes cluster using a Helm chart 

## Technologies Used 

* Kubernetes 

* Helm 

* Prometheus 

## Steps 

Step 1: Add prometheus community repo 

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

[Adding repo](/images/01_install_chart.png)

Step 2: Update helm repo 

    helm repo update

Step 3: Install kube prometheus stack chart as this will container prometheus operator and more in it

    helm install prometheus prometheus-community/kube-prometheus-stack

[Installed chart](/images/01_install_chart.png)
[Other Components](/images/02_components_that_was_created.png)


## Installation

Run $ brew install minikube 


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-prometheus-monitoring-in-cluster.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-prometheus-monitoring-in-cluster

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.